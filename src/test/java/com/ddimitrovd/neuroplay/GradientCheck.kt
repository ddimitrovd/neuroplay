package com.ddimitrovd.neuroplay

import com.ddimitrovd.neuroplay.data.DatasetManager
import com.ddimitrovd.neuroplay.neural.NeuralNetwork
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Test
import kotlin.math.abs

import kotlin.math.sin
import kotlin.random.Random

/**
 * Comparing the gradients (partial derivatives) computed by the brackprop against numerically computed gradients
 * This can let us know hot the back prop implementation is doing and if there are any major problems
 * It only needs to be run if backprop or any related methods are changed
 */
class GradientCheck {

    private val e = 1e-3f // the offset value of the interval
    private val differenceThreshold = 1e-3 // acceptable difference between the backporp gradients and the numerically computed ones

    private val inputLayerSize = 3
    private val numberExamples = 5
    private var numberHiddenLayers = 1
    private var unitsPerLayer = intArrayOf(5)

    @Test
    fun checkGradFor1Layer() {
        numberHiddenLayers = 1
        unitsPerLayer = intArrayOf(5)
        checkGrad()
    }

    @Test
    fun checkGradFor2Layer() {
        numberHiddenLayers = 2
        unitsPerLayer = intArrayOf(4,7)
        checkGrad()
    }

    @Test
    fun checkGradFor3Layer() {
        numberHiddenLayers = 3
        unitsPerLayer = intArrayOf(3,5,8)
        checkGrad()
    }


    private fun checkGrad() {
        DatasetManager.dataset.trainingData= initializeInput()
        DatasetManager.dataset.trainingLabels= initializeLabels()
        NeuralNetwork.setUpNetwork(numberHiddenLayers, unitsPerLayer, 0,0f,0f)

        val backpropGradients = NeuralNetwork.wBackpropGradTest()
        val numericGradients = calculateNumericGradients(backpropGradients)

        for (w in backpropGradients!!.indices) {
            for (r in backpropGradients[w]!!.indices) {
                for (c in backpropGradients[w]!![r].indices) {
                    val diff = backpropGradients[w]!![r][c] - numericGradients[w][r][c]
                    assertTrue(abs(diff.toDouble()) < differenceThreshold)
                }
            }
        }
    }


    private fun calculateNumericGradients(gradsArray: Array<Array<FloatArray>?>?): ArrayList<ArrayList<ArrayList<Float>>>{
        val grads = ArrayList<ArrayList<ArrayList<Float>>>()
        // go over each individual weight
        for (w in gradsArray!!.indices) {
            val rows = ArrayList<ArrayList<Float>>()
            for (r in gradsArray[w]!!.indices) {
                val cols = ArrayList<Float>()
                for (c in gradsArray[w]!![r].indices) {
                    val costA = NeuralNetwork.wCostWithWeightModifTest(w,r,c,-e)
                    val costB = NeuralNetwork.wCostWithWeightModifTest(w,r,c,e)
                    cols.add((costB-costA)/(2f*e))
                }
                rows.add(cols)
            }
            grads.add(rows)
        }
        return grads
    }


    private fun initializeInput(): ArrayList<FloatArray> {
        val arrayList = ArrayList<FloatArray>(numberExamples)
        for (i in 0 until numberExamples) {
            arrayList.add(FloatArray(inputLayerSize+1) { j -> Random.nextDouble(0.0, 1.0).toFloat()})
        }
        return arrayList
    }


    private fun initializeLabels() : ArrayList<FloatArray>{
        val arrayList = ArrayList<FloatArray>(numberExamples)
        for (i in 0 until numberExamples) {
            arrayList.add(vectorizedLabel(i))
        }
        return arrayList
    }


    private fun vectorizedLabel(i: Int): FloatArray {
        return when (i%3) {
            0-> floatArrayOf(0f,0f,1f)
            1-> floatArrayOf(0f,1f,0f)
            else-> floatArrayOf(1f,0f,0f)
        }
    }
}
