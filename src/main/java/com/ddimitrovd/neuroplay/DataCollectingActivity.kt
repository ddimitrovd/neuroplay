package com.ddimitrovd.neuroplay

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Canvas
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.ddimitrovd.neuroplay.data.DatasetManager
import com.ddimitrovd.neuroplay.data.getBitmapFromGrayscaleIntArray
import kotlinx.android.synthetic.main.activity_data_collecting.*
import java.lang.Exception
import kotlin.random.Random

class DataCollectingActivity : AppCompatActivity() {

    private var label = 0
    private var isCollectingTest = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_data_collecting)

        setClickListeners()
        setLabelIndicationText()
        setNumberOfExamplesIndicationText()
        Toast.makeText(this,getString(R.string.data_collecting_provide_trainingset_toast_suggestion), Toast.LENGTH_SHORT).show()
    }


    override fun onBackPressed() {
        // Go back to provide set
        startActivity(Intent(this, ProvideSetActivity::class.java))
    }


    private fun setClickListeners() {
        //Done
        data_progression_btn.setOnClickListener {
            if (!data_collecting_draw_view.isClean) collectInput() //collect only if there is something drawn
            if (!isCollectingTest) { // Is currently collecting training set
                startCollectingTestset()
            } else { // Is currently collecting test set
                DatasetManager.saveCustomDataset(this)
                goToNextActivity()
            }
        }

        // Continue
        data_collecting_continue_btn.setOnClickListener {
            collectInput() // collect even if canvas is blank - user freedom
        }

        //Clear
        data_collecting_clear_btn.setOnClickListener {
            clearCanvas()
        }
    }


    private fun collectInput() {
        saveDrawingInDataset()
        previewAugmentationsFormLastExample()
        clearCanvas()
        nextLabel()
        setLabelIndicationText()
        setNumberOfExamplesIndicationText()
        setProgressionBtnState()
    }


    //region ----- Collection progression ------------------------------------------------------------------------------

    private fun startCollectingTestset() {
        isCollectingTest = true
        setProgressionBtnState()
        Toast.makeText(this,getString(R.string.data_collecting_provide_testset_toast_suggestion), Toast.LENGTH_SHORT).show()
    }


    private fun goToNextActivity() {
        // Go to next Activity (Architecture)
        startActivity(Intent(this, ArchitectureActivity::class.java))
    }

    //endregion


    //region ----- User action indications and status ------------------------------------------------------------------


    @SuppressLint("SetTextI18n")
    private fun setLabelIndicationText() {
        data_collecting_label.text = when (label){
            0 -> DatasetManager.dataset.classes[0]
            1 -> DatasetManager.dataset.classes[1]
            2 -> DatasetManager.dataset.classes[2]
            else -> throw Exception("Class index out of bounds")
        }
    }


    @SuppressLint("SetTextI18n")
    private fun setNumberOfExamplesIndicationText() {
        // Trainingset
        if (!isCollectingTest) { // update only if collecting trainingset
            data_collecting_size_of_training_text.text =
                resources.getString(R.string.data_collecting_number_of_examples_in_training_indication) + " " + DatasetManager.dataset.trainingData.size
        }
        // Testset
        data_collecting_size_of_test_text.text =
            resources.getString(R.string.data_collecting_number_of_examples_in_test_indication) + " " + DatasetManager.dataset.testData.size
    }


    private fun setProgressionBtnState() {
        if (!isCollectingTest){ // We are currently collecting trainingset
            if (DatasetManager.dataset.trainingData.isNotEmpty()){ // We have collected some trainingData
                data_progression_btn.isEnabled = true
            }
        } else { // We are currently collecting testset
            if (DatasetManager.dataset.testData.isNotEmpty()){// We have collected some trainingData
                data_progression_btn.isEnabled = true
            } else { // We have no trainingData so far
                data_progression_btn.text = getString(R.string.draw_done)
                data_progression_btn.isEnabled = false
            }
        }
    }


    private fun previewAugmentationsFormLastExample() {
        val data = if (isCollectingTest) DatasetManager.dataset.testData else DatasetManager.dataset.trainingData

        if (data.size < NUMBER_OF_AUGMENTATIONS) return

        val imageViews = arrayOf(
            data_collecting_preview1_image_view,
            data_collecting_preview2_image_view,
            data_collecting_preview3_image_view,
            data_collecting_preview4_image_view,
            data_collecting_preview5_image_view)

        for (element in imageViews) {
            val exampleIndex = Random.nextInt(data.size - NUMBER_OF_AUGMENTATIONS,data.size)
            element.setImageBitmap(getBitmapFromGrayscaleIntArray(data[exampleIndex]))
        }
    }


    private fun nextLabel() {
        label = ++label % 3
    }

    //endregion


    //region ----- Send to dataset -------------------------------------------------------------------------------------

    private fun saveDrawingInDataset() {
        val bitmap = Bitmap.createBitmap(data_collecting_draw_view.width, data_collecting_draw_view.height, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        data_collecting_draw_view.draw(canvas)
        DatasetManager.addBitmapToDataset(bitmap, getVectorLabel(), isCollectingTest)
    }


    private fun getVectorLabel(): FloatArray {
        return when (label) {
            0 -> floatArrayOf(1f, 0f, 0f)
            1 -> floatArrayOf(0f, 1f, 0f)
            2 -> floatArrayOf(0f, 0f, 1f)
            else -> throw Exception("Class index out of bounds")
        }
    }

    //endregion


    private fun clearCanvas() {
        data_collecting_draw_view.clearCanvas()
    }

}