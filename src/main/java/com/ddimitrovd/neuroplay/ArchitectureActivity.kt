package com.ddimitrovd.neuroplay

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.text.TextUtils
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.Toast
import com.ddimitrovd.neuroplay.neural.NeuralNetwork
import com.ddimitrovd.neuroplay.neural.OptimizationService
import kotlinx.android.synthetic.main.activity_architecture.*

// TODO implement input checking

class ArchitectureActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_architecture)
        setClickListeners()
        initInputFieldsWithDefaults()
        toastHelpHint()
    }


    override fun onBackPressed() {
        // Go back to provide set
        startActivity(Intent(this, ProvideSetActivity::class.java))
    }


    private fun setClickListeners() {
        architecture_continue_btn.setOnClickListener {
            setUpNetwork()
            startLearning()
        }
        architecture_layers_radio_group.setOnCheckedChangeListener {_, i -> selectedNumberOfLayers(i)}

        // options text click for more details in alert dialog
        architecture_help_image_btn.setOnClickListener { toastHelpHint() }
        architecture_number_of_layers_text.setOnClickListener {showInformation(R.string.architecture_number_of_layers_selection, R.string.architecture_number_of_layers_explanation)}
        architecture_units_per_layer_text.setOnClickListener {showInformation(R.string.architecture_units_per_layer_selection, R.string.architecture_units_per_layer_explanation)}
        architecture_iterations_text.setOnClickListener {showInformation(R.string.architecture_iterations_selection, R.string.architecture_iterations_explanation)}
        architecture_learning_rate_text.setOnClickListener {showInformation(R.string.architecture_learning_rate_selection, R.string.architecture_learning_rate_explanation)}
        architecture_regularization_parameter_text.setOnClickListener {showInformation(R.string.architecture_regularization_parameter_selection, R.string.architecture_regularization_parameter_explanation)}
    }


    private fun setUpNetwork() {
        NeuralNetwork.setUpNetwork(getNumberOfLayersSelected(),getArrayOfNumberOfUnits(),getIterations(),getLearningRate(),getRegularizationParameter())
    }


    private fun startLearning() {
        // start foreground service
        startForegroundService(Intent(this, OptimizationService::class.java)
            .putExtra(EXTRA_ITERATION_OPTIMUM_CHECK, architecture_iterations_optimum_check_switch.isChecked))
        // Go to next activity (Learning Activity)
        startActivity(Intent(this, LearningActivity::class.java))
    }


    //region ----- Explanations / Information --------------------------------------------------------------------------

    private fun showInformation(titleResID: Int, messageResID: Int) {
        val dialogBuilder = AlertDialog.Builder(this, R.style.InformationDialog)
        dialogBuilder.setMessage(getString(messageResID))
            .setCancelable(true)
        val alert = dialogBuilder.create()
        alert.setTitle(getString(titleResID))
        alert.show()
    }


    private fun toastHelpHint() {
        Toast.makeText(this,R.string.architecture_text_click_hint,Toast.LENGTH_LONG).show()
    }

    //endregion


    //region ----- Input Checking --------------------------------------------------------------------------------------

    private fun selectedNumberOfLayers(i: Int) {
        when (findViewById<RadioButton>(i)){
            architecture_two_layers_radio_btn -> {
                architecture_units_in_second_layer_edit_text.isEnabled=true
                architecture_units_in_third_layer_edit_text.isEnabled=false
            }
            architecture_three_layers_radio_btn -> {
                architecture_units_in_second_layer_edit_text.isEnabled=true
                architecture_units_in_third_layer_edit_text.isEnabled=true
            }
            else -> {
                architecture_units_in_second_layer_edit_text.isEnabled=false
                architecture_units_in_third_layer_edit_text.isEnabled=false
            }
        }
    }

    //endregion


    //region ----- Fields initialization -------------------------------------------------------------------------------

    private fun initInputFieldsWithDefaults() {
        architecture_units_in_first_layer_edit_text.setText(DEFAULT_NUMBER_OF_HIDDEN_UNITS[0].toString())
        architecture_units_in_second_layer_edit_text.setText(DEFAULT_NUMBER_OF_HIDDEN_UNITS[1].toString())
        architecture_units_in_third_layer_edit_text.setText(DEFAULT_NUMBER_OF_HIDDEN_UNITS[2].toString())
        architecture_iterations_edit_text.setText(DEFAULT_NUMBER_OF_ITERATIONS.toString())
        architecture_learning_rate_edit_text.setText(DEFAULT_LEARNING_RATE.toString())
        architecture_regularization_parameter_edit_text.setText(DEFAULT_REGULARIZATION.toString())
    }

    //endregion


    // region ----- Input Getters---------------------------------------------------------------------------------------


    private fun getNumberOfLayersSelected(): Int {
        if (architecture_one_layer_radio_btn.isChecked)
            return 1
        if (architecture_two_layers_radio_btn.isChecked)
            return 2
        if (architecture_three_layers_radio_btn.isChecked)
            return 3
        else
            throw Exception("Architecture number of layers not selected")
    }


    private fun getArrayOfNumberOfUnits(): IntArray {
        // If empty return default
        if (TextUtils.isEmpty(architecture_units_in_first_layer_edit_text.text)
            || TextUtils.isEmpty(architecture_units_in_second_layer_edit_text.text)
            || TextUtils.isEmpty(architecture_units_in_third_layer_edit_text.text)) {
            return DEFAULT_NUMBER_OF_HIDDEN_UNITS
        } else {
            val first = architecture_units_in_first_layer_edit_text.text.toString().toInt()
            val second = architecture_units_in_second_layer_edit_text.text.toString().toInt()
            val third = architecture_units_in_third_layer_edit_text.text.toString().toInt()
            when (getNumberOfLayersSelected()) {
                1 -> return intArrayOf(first)
                2 -> return intArrayOf(first, second)
                3 -> return intArrayOf(first, second, third)
            }
        }
        throw Exception("Architecture number of units not correct")
    }


    private fun getIterations(): Int {
        // If empty return default
        if (TextUtils.isEmpty(architecture_iterations_edit_text.text)) return DEFAULT_NUMBER_OF_ITERATIONS
        return architecture_iterations_edit_text.text.toString().toInt()
    }


    private fun getLearningRate(): Float {
        // If empty or 0 return default
        if (TextUtils.isEmpty(architecture_learning_rate_edit_text.text)
            || architecture_learning_rate_edit_text.text.toString().toFloat() == 0f) return DEFAULT_LEARNING_RATE
        return architecture_learning_rate_edit_text.text.toString().toFloat()
    }


    private fun getRegularizationParameter(): Float {
        // If empty return default
        if (TextUtils.isEmpty(architecture_regularization_parameter_edit_text.text)) return DEFAULT_REGULARIZATION
        return architecture_regularization_parameter_edit_text.text.toString().toFloat()
    }

    // endregion



}