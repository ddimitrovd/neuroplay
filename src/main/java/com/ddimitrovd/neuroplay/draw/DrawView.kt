/**
 * This file's contents, as used in the package com.ddimitrovd.neuroplay.draw,
 * are mostly taken from https://android.jlelse.eu/a-guide-to-drawing-in-android-631237ab6e28
 */
package com.ddimitrovd.neuroplay.draw

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Path
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import android.graphics.Bitmap
import com.ddimitrovd.neuroplay.DRAW_STROKE_SIZE

/**
 * This class is only responsible for the drawing itself and controls the drawing view
*/
class DrawView(context: Context, attrs: AttributeSet) : View(context, attrs) {

    var isClean = true

    private var mPaint = Paint()
    private var mPath = Path()

    // variables for path creation
    private var mCurX = 0f
    private var mCurY = 0f
    private var mStartX = 0f
    private var mStartY = 0f


    init {
        mPaint.apply {
            // drawing parameters
            color = Color.BLACK
            style = Paint.Style.STROKE
            strokeJoin = Paint.Join.ROUND
            strokeCap = Paint.Cap.ROUND
            strokeWidth = DRAW_STROKE_SIZE
            isAntiAlias = true
        }
    }


    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        canvas.drawPath(mPath, mPaint)
    }


    private fun actionDown(x: Float, y: Float) {
        mPath.moveTo(x, y)
        mCurX = x
        mCurY = y
        isClean = false
    }


    private fun actionMove(x: Float, y: Float) {
        mPath.quadTo(mCurX, mCurY, (x + mCurX) / 2, (y + mCurY) / 2)
        mCurX = x
        mCurY = y
    }


    private fun actionUp() {
        mPath.lineTo(mCurX, mCurY)

        // draw a dot on click
        if (mStartX == mCurX && mStartY == mCurY) {
            mPath.lineTo(mCurX, mCurY + 2)
            mPath.lineTo(mCurX + 1, mCurY + 2)
            mPath.lineTo(mCurX + 1, mCurY)
        }
    }


    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent): Boolean {
        val x = event.x
        val y = event.y

        when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                mStartX = x
                mStartY = y
                actionDown(x, y)
            }
            MotionEvent.ACTION_MOVE -> actionMove(x, y)
            MotionEvent.ACTION_UP -> actionUp()
        }
        invalidate()
        return true
    }


    fun clearCanvas() {
        mPath.reset()
        invalidate()
        isClean = true
    }
}