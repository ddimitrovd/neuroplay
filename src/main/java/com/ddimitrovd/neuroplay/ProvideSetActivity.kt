package com.ddimitrovd.neuroplay

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Toast
import com.ddimitrovd.neuroplay.data.DatasetManager
import kotlinx.android.synthetic.main.activity_provide_set.*
import java.lang.Exception

class ProvideSetActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_provide_set)

        provide_set_part2_const_layout.visibility = View.INVISIBLE
        setClickListeners()
    }


    override fun onBackPressed() {
        // Go back to main menu
        startActivity(Intent(this, MainActivity::class.java))
    }


    private fun setClickListeners() {
        provide_set_continue_btn.setOnClickListener {
            // Continue to part 2
            DatasetManager.clearDataset()
            saveClasses()
            goToPart2()
        }
        provide_set_draw_btn.setOnClickListener {
            goToNextActivity()
        }
        provide_set_load_set_btn.setOnClickListener {
            loadDataset()
        }
    }


    private fun goToPart2() {
        provide_set_part2_const_layout.visibility = View.VISIBLE
        provide_set_class1_edit_text.isEnabled = false
        provide_set_class2_edit_text.isEnabled = false
        provide_set_class3_edit_text.isEnabled = false
        provide_set_continue_btn.isEnabled = false
    }


    private fun saveClasses() {
        DatasetManager.dataset.setClassesNames(
            arrayOf(resources.getString(R.string.provide_set_class1),
                resources.getString(R.string.provide_set_class2),
                resources.getString(R.string.provide_set_class3)),
            arrayOf(provide_set_class1_edit_text.text.toString(),
                provide_set_class2_edit_text.text.toString(),
                provide_set_class3_edit_text.text.toString()))
    }


    private fun goToNextActivity(isDatasetLoaded: Boolean = false) {
        if (isDatasetLoaded) {
            // Go to Architecture as dataset is already loaded
            startActivity(Intent(this, ArchitectureActivity::class.java))
        } else {
            // Go to Data Collection to start collecting dataset
            startActivity(Intent(this, DataCollectingActivity::class.java))
        }
    }

    //region ----- load saved dataset ----------------------------------------------------------------------------------

    // Temporary loading function until full featured save/load is implemented
    private fun loadDataset() {
        try {
            DatasetManager.loadCustomDataset(this)
            val train = DatasetManager.dataset.trainingData.size
            val test = DatasetManager.dataset.testData.size
            Toast.makeText(this,"LOADED:\ntraining set size:$train\ntest set size:$test ", Toast.LENGTH_LONG).show()
            goToNextActivity(true)
        } catch (e: Exception) {
            Toast.makeText(this,"No saved file was found, first provide a dataset manually, next time you open the app you should be able to open it", Toast.LENGTH_LONG).show()
        }
    }

    //endregion
}