package com.ddimitrovd.neuroplay

import android.annotation.SuppressLint
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*
import android.net.Uri


class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        createNotificationChannel()
        setClickListeners()
        setVersionName()
    }


    private fun setClickListeners() {
        // Go to creating a custom training set
        main_manual_train_btn.setOnClickListener {
            // Go to next activity (Provide Set)
            startActivity(Intent(this, ProvideSetActivity::class.java))
        }
        main_how_to_btn.setOnClickListener {
            navigateToURI(getString(R.string.link_how_to_use))
        }
        main_about_btn.setOnClickListener {
            navigateToURI(getString(R.string.link_about))
        }
    }


    private fun createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        val name = getString(R.string.notification_chanel_name)
        val descriptionText = getString(R.string.notification_chanel_description)
        val importance = NotificationManager.IMPORTANCE_DEFAULT
        val channel = NotificationChannel(NOTIFICATION_CHANNEL_ID, name, importance).apply {
            description = descriptionText
        }
        // Register the channel with the system
        val notificationManager: NotificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.createNotificationChannel(channel)
    }


    @SuppressLint("SetTextI18n")
    private fun setVersionName() {
        val version =  packageManager.getPackageInfo(packageName, 0).versionName
        main_version_text.text = getString(R.string.version_label) + " " + version
    }


    private fun navigateToURI(url: String) {
        startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(url)))
    }

}
