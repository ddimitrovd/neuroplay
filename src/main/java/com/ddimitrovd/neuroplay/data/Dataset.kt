package com.ddimitrovd.neuroplay.data

import java.io.Serializable

/**
 * This object contains the dataset currently in use
 * @property name placed for future compatibility for multiple saved dataset feature
 */
class Dataset (var name:String) : Serializable{

    var classes = arrayOf<String>()
    var trainingLabels = ArrayList<FloatArray>()
    var trainingData = ArrayList<FloatArray>()
    var testLabels = ArrayList<FloatArray>()
    var testData = ArrayList<FloatArray>()


    /**
     * Set the classes names
     * @param default names if no given name by user
     * @param givenNames names given by user
     */
    fun setClassesNames(defaultNames : Array<String>, givenNames: Array<String>) {
        val n0 = if (givenNames[0] == "") defaultNames[0] else givenNames[0]
        val n1 = if (givenNames[1] == "") defaultNames[1] else givenNames[1]
        val n2 = if (givenNames[2] == "") defaultNames[2] else givenNames[2]
        classes = arrayOf(n0,n1,n2)
    }
}