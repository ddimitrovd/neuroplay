package com.ddimitrovd.neuroplay.data

import android.content.Context
import android.graphics.Bitmap
import com.ddimitrovd.neuroplay.*
import java.io.ObjectInputStream
import java.io.ObjectOutputStream

/**
 * This object saves, loads and transforms the trainingData in the dataset
 */
object DatasetManager {

    var dataset = Dataset(DEFAULT_DATASET_NAME)


    /**
     * Converts the image to the used trainingData format and saves it to the dataset
     * @param bitmap the input image
     * @return the class corresponding to the image
     */
    fun addBitmapToDataset(bitmap: Bitmap, label: FloatArray, isTestExample: Boolean = false) {
        val resizedBitmap = resizeBitmapToDatasetFormat(bitmap)
        // Augment with resized map to the desired dataset dimensions
        val augmentedData = augmentData(resizedBitmap)

        // Save data
        for (augData in augmentedData) { // augmented input
            if (!isTestExample) addFloatArrayToTrainingDataset(getScaledFloatArrayFromBitmap(augData), label)
            if (isTestExample) addFloatArrayToTestDataset(getScaledFloatArrayFromBitmap(augData), label)
        }
    }


    /**
     * Adds example and label pair to the training set
     */
    private fun addFloatArrayToTrainingDataset(data: FloatArray, label: FloatArray) {
        dataset.trainingLabels.add(label)
        dataset.trainingData.add(data)
    }


    /**
     * Adds example and label pair to the test set
     */
    private fun addFloatArrayToTestDataset(data: FloatArray, label: FloatArray) {
        dataset.testLabels.add(label)
        dataset.testData.add(data)
    }


    /**
     * Run the available augmentations on a bitmap in order to multiply dataset
     * @param bitmap trainingData to be augmented
     * @return ArrayList of the augmented trainingData of size config.NUMBER_OF_AUGMENTATIONS
     */
    private fun augmentData(bitmap: Bitmap): ArrayList<Bitmap> {
        val augmentedData = ArrayList<Bitmap>()
        augmentedData.add(bitmap)

        // Rotation on original
        for (deg in ROTATION_AUGMENTATION_DEGREES) {
            //resize after rotate as rotation might alter size
            augmentedData.add(resizeBitmapToDatasetFormat(rotateBitmap(bitmap, deg)))
        }

        // Zoom out on all previous examples augmented and original
        for (i in augmentedData.indices) {
            for (factor in ZOOM_OUT_AUGMENTATION_FACTORS) {
                augmentedData.add(zoomOutBitmap(augmentedData[i], factor))
            }
        }

        // Skew on all previous examples augmented and original
        for (i in augmentedData.indices) {
            for (x in SKEW_TILT_X_AUGMENTATION_FACTORS) {
                for (y in SKEW_TILT_Y_AUGMENTATION_FACTORS) {
                    //resize after skew as skewing might alter size
                    if (x != 0f || y != 0f) augmentedData.add(resizeBitmapToDatasetFormat(skewBitmap(augmentedData[i], x, y)))
                }
            }
        }

        // Translate on all previous examples augmented and original
        for (i in augmentedData.indices) {

            for (x in TRANSLATE_X_AUGMENTATION_FACTORS) {
                for (y in TRANSLATE_Y_AUGMENTATION_FACTORS) {
                    if (x != 0f || y != 0f) augmentedData.add(translateBitmap(augmentedData[i],x,y))
                }
            }
        }

        return augmentedData
    }

    //region ----- load/save -------------------------------------------------------------------------------------------
    //TODO implement load/save system for many datasets

    fun loadCustomDataset(context: Context) {
        val fis = context.openFileInput("numbersDataset")
        val ois = ObjectInputStream(fis)
        val ds = ois.readObject() as Dataset
        ois.close()
        fis.close()
        dataset = ds
    }


    fun saveCustomDataset(context: Context) {
        val fos = context.openFileOutput("numbersDataset", Context.MODE_PRIVATE)
        val os = ObjectOutputStream(fos)
        os.writeObject(dataset)
        os.close()
        fos.close()
    }


    fun clearDataset() {
        dataset = Dataset(DEFAULT_DATASET_NAME)
    }

    //endregion
}