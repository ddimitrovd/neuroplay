package com.ddimitrovd.neuroplay.data

import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Matrix
import com.ddimitrovd.neuroplay.DATA_SIZE
import kotlin.math.abs


/**
 * Get scaled pixel values from bitmap if it is not the correct size DATA_SIZE x DATA_SIZE it will be resized
 * @return IntArray with values 1 for black and 0 for white pixel
 */
fun getScaledFloatArrayFromBitmap(bitmap: Bitmap) : FloatArray {
    // check if bitmap is with good size DATA_SIZE x DATA_SIZE
    val checkedSizeBitmap = if(bitmap.width != DATA_SIZE || bitmap.height != DATA_SIZE)
        resizeBitmapToDatasetFormat(bitmap) else bitmap

    val matrixSize = DATA_SIZE*DATA_SIZE
    val pixels = IntArray(matrixSize)
    checkedSizeBitmap.getPixels(pixels, 0, checkedSizeBitmap.width, 0, 0, DATA_SIZE, DATA_SIZE)
    return grayscaleParameterScaledFloatArray(pixels)
}


/**
 * Resize (change number of pixels by keeping the same image and aspect ratio, basically change resolution)
 * the bitmap to the simplified size specified in the config file
 * This will improve performance, as the input layer will be smaller
 */
fun resizeBitmapToDatasetFormat(bitmap: Bitmap) : Bitmap {
    return Bitmap.createScaledBitmap(bitmap, DATA_SIZE, DATA_SIZE, false)
}


/**
 * Rotate a bitmap with a matrix rotation
 * @param bitmap the bitmap to be rotated
 * @param degrees degrees of rotation as float
 */
fun rotateBitmap(bitmap: Bitmap, degrees: Float) : Bitmap {
    val matrix = Matrix()
    matrix.postRotate(degrees)
    return Bitmap.createBitmap(bitmap, 0, 0, bitmap.width, bitmap.height, matrix, true)
}


/**
 * Zoom out the image by keeping the original size. It basically shrinks the image and then overlays it on a white canvas
 * @param factor should be between 1 and 2 (1 = no change) (2 = 2x zoom out)
 */
fun zoomOutBitmap(bitmap: Bitmap, factor: Float) : Bitmap {
    if (factor < 1 || factor > 2) throw Exception("Zoom out factor out of bounds")

    // Get white and resized bitmaps
    val zoomedSize = (DATA_SIZE/factor).toInt()
    val zoomedBitmap = Bitmap.createScaledBitmap(bitmap, zoomedSize, zoomedSize, false)
    val hostBitmap = Bitmap.createBitmap(DATA_SIZE, DATA_SIZE, Bitmap.Config.ARGB_8888)
    hostBitmap.eraseColor(Color.WHITE)

    // Draw both on canvas with host bitmap by offsetting small image to be centered
    val canvas = Canvas(hostBitmap)
    canvas.drawBitmap(hostBitmap, 0f, 0f, null)
    val offset = ((DATA_SIZE - zoomedSize)/2).toFloat()
    canvas.drawBitmap(zoomedBitmap, offset, offset, null)
    return hostBitmap
}


/**
 * Translate the image on the x, y plane
 * @param x translation by x pixels on the x horizontal axis
 * @param y translation by y pixels on the y vertical axis
 */
fun translateBitmap(bitmap: Bitmap, x: Float, y: Float): Bitmap {
    if (abs(x) >= DATA_SIZE || abs(y) >= DATA_SIZE) throw Exception("Translation x or y out of bounds")

    val hostBitmap = Bitmap.createBitmap(DATA_SIZE, DATA_SIZE, Bitmap.Config.ARGB_8888)
    hostBitmap.eraseColor(Color.WHITE)
    val canvas = Canvas(hostBitmap)
    canvas.drawBitmap(hostBitmap, 0f, 0f, null)
    canvas.translate(x,y)
    canvas.drawBitmap(bitmap, 0f, 0f, null)
    return hostBitmap
}


/**
 * Skew (Distort) the image on the x and y axis
 * @param x distortion on the x horizontal axis
 * @param y distortion on the y vertical axis
 */
fun skewBitmap(bitmap: Bitmap, x: Float, y: Float) : Bitmap {
    val matrix = Matrix()
    val center = DATA_SIZE/2f
    matrix.setSkew(x, y, center, center)
    return Bitmap.createBitmap(bitmap, 0, 0, bitmap.width, bitmap.height, matrix, true)
}


/**
 * Return a grayscale floatArray equivalent of a ARGB_8888 bitmap intArray with values scaled in range 0 to 1 (1 = white, 0 = black)
 */
fun grayscaleParameterScaledFloatArray(argbArray: IntArray) : FloatArray {
    // although G,B and R values should all be equal, just in case take average
    val grayArray = FloatArray(argbArray.size)
    for (i in argbArray.indices) {
        val alpha = argbArray[i] shr 24
        val red = (argbArray[i] and 0x00FF0000) shr 16
        val green = (argbArray[i] and 0x0000FF00) shr 8
        val blue = (argbArray[i] and 0x000000FF)
        grayArray[i] = if (alpha == -1) ((red+green+blue)/3).toFloat()/255 else 1f
    }
    return grayArray
}


/**
 * Return a ARGB_8888 bitmap form a grayscaled floatArray with values 0 to 1
 */
fun getBitmapFromGrayscaleIntArray(floatArray: FloatArray) : Bitmap {
    val bitmap = Bitmap.createBitmap(DATA_SIZE, DATA_SIZE, Bitmap.Config.ARGB_8888)
    val argbArray = IntArray(floatArray.size)
    for (i in floatArray.indices) {
        val intValue = (floatArray[i]*255).toInt()
        argbArray[i] = Color.argb(255, intValue, intValue, intValue)
    }
    bitmap.setPixels(argbArray, 0, DATA_SIZE, 0, 0, DATA_SIZE, DATA_SIZE)
    return bitmap
}