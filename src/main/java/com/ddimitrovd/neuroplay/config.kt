package com.ddimitrovd.neuroplay

/**
 * This file hold global configuration related constants for the whole application. It is not in res as some constants
 * need to be accessed outside of context
 */

const val NOTIFICATION_CHANNEL_ID = "NeuroplayNotificationChannel"

const val BROADCAST_PROGRESS_NAME = "LearningProgress"
const val BROADCAST_COMPLETED_NAME = "LearningCompleted"

const val EXTRA_ITERATION_NAME = "iteration"
const val EXTRA_ITERATION_OPTIMUM_CHECK = "hasOptimum"
const val EXTRA_COST_NAME = "cost"
const val EXTRA_PROGRESS_NAME = "progress"

const val DRAW_STROKE_SIZE = 100f

const val DEFAULT_DATASET_NAME = "defaultDataset"

const val DATA_SIZE = 20
const val NUMBER_OF_CLASSES = 3

const val NUMBER_OF_AUGMENTATIONS = 90 //TODO remove when implementing user defined augmentations
val ROTATION_AUGMENTATION_DEGREES = arrayOf(6f,-6f,10f,-10f)
val ZOOM_OUT_AUGMENTATION_FACTORS = arrayOf(1.2f)
val SKEW_TILT_X_AUGMENTATION_FACTORS = arrayOf(0f, 0.2f, -0.2f)
val SKEW_TILT_Y_AUGMENTATION_FACTORS = arrayOf(0f)
val TRANSLATE_X_AUGMENTATION_FACTORS = arrayOf(0f, 2f, -2f)
val TRANSLATE_Y_AUGMENTATION_FACTORS = arrayOf(0f)

val DEFAULT_NUMBER_OF_HIDDEN_UNITS = intArrayOf(20,20,20)
const val DEFAULT_NUMBER_OF_HIDDEN_LAYERS = 1
const val DEFAULT_LEARNING_RATE = 0.125f
const val DEFAULT_REGULARIZATION = 2f
const val DEFAULT_NUMBER_OF_ITERATIONS = 100

const val GOOD_TRAINING_ACCURACY_THRESHOLD = 80f
const val GOOD_TEST_ACCURACY_DIFFERENCE_THRESHOLD = 6f

