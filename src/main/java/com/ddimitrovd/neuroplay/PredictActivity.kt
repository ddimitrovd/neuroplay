package com.ddimitrovd.neuroplay

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Canvas
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.ddimitrovd.neuroplay.data.DatasetManager
import com.ddimitrovd.neuroplay.data.getScaledFloatArrayFromBitmap
import com.ddimitrovd.neuroplay.neural.NeuralNetwork
import kotlinx.android.synthetic.main.activity_data_collecting.*
import kotlinx.android.synthetic.main.activity_predict.*

class PredictActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_predict)
        setClickListeners()
        setClassesNames()
    }


    override fun onBackPressed() {
        // Go back to provide set
        startActivity(Intent(this, ProvideSetActivity::class.java))
    }


    private fun setClickListeners() {
        predict_recognize_btn.setOnClickListener {
            recogniseInput()
            clearCanvas()
        }
    }


    private fun setClassesNames() {
        predict_class1_name_text.text = DatasetManager.dataset.classes[0]
        predict_class2_name_text.text = DatasetManager.dataset.classes[1]
        predict_class3_name_text.text = DatasetManager.dataset.classes[2]
    }


    private fun clearCanvas() {
        predict_draw_view.clearCanvas()
    }


    private fun getFloatArrayFromDrawing(): FloatArray {
        val bitmap = Bitmap.createBitmap(predict_draw_view.width, predict_draw_view.height, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        predict_draw_view.draw(canvas)
        return getScaledFloatArrayFromBitmap(bitmap)
    }


    private fun recogniseInput() {
        val output = NeuralNetwork.predict(getFloatArrayFromDrawing())
        setOutputValues(output)
        choseBestPrediction(output)
    }


    private fun setOutputValues(output: FloatArray) {
        predict_class1_value_text.text = output[0].toString()
        predict_class2_value_text.text = output[1].toString()
        predict_class3_value_text.text = output[2].toString()
    }


    @SuppressLint("SetTextI18n")
    private fun choseBestPrediction(output: FloatArray) {
        val max = output.indexOf(output.max()!!)
        predict_class_indication_text.text = getString(R.string.predict_recognition_text) + " " + DatasetManager.dataset.classes[max] + "."
    }

}