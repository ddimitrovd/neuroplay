package com.ddimitrovd.neuroplay

import android.annotation.SuppressLint
import android.app.NotificationManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.ddimitrovd.neuroplay.neural.NeuralNetwork
import kotlinx.android.synthetic.main.activity_learning.*

class LearningActivity : AppCompatActivity() {

    private val broadCastReceiver = object : BroadcastReceiver() {
        override fun onReceive(contxt: Context?, intent: Intent?) {
            when (intent?.action) {
                BROADCAST_PROGRESS_NAME -> updateProgress(intent)
                BROADCAST_COMPLETED_NAME -> learningComplete()
            }
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_learning)
        setClickListeners()
    }


    override fun onResume() {
        super.onResume()
        //register receiver
        registerReceiver(broadCastReceiver, IntentFilter(BROADCAST_PROGRESS_NAME))
        registerReceiver(broadCastReceiver, IntentFilter(BROADCAST_COMPLETED_NAME))

        // check if system dumped app memory TODO: save learned parameters state
        if (!NeuralNetwork.isInitialized()) {
            learning_progress_title_text.text = "Well the cute dev hasn't yet implemented saving the current status of the neural network (he will eventually) and the system has dumped the memory. So we forgot all you did. Sorry! Try to be faster and open the app immediately after the learning is complete"
            learning_results_linear_layout.visibility = View.GONE
            return
        }

        // resuming has to check what is going on
        setLoadingBarsVisibility()
        setResultsVisibility()
        if (NeuralNetwork.isLearningDone) {
            // learning was completed while activity was paused
            learningComplete()
        }
    }


    override fun onPause() {
        super.onPause()
        // unregister to avoid memory leak
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadCastReceiver)
    }


    override fun onDestroy() {
        super.onDestroy()
        // unregister to avoid memory leak
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadCastReceiver)
    }


    override fun onBackPressed() {
        // No going back!
    }


    private fun setClickListeners() {
        learning_continue_btn.setOnClickListener {
            dismissNotifications()
            // Go to next activity (Predict)
            startActivity(Intent(this, PredictActivity::class.java))
        }
        learning_abort_btn.setOnClickListener {
            NeuralNetwork.isAborting = true
            dismissNotifications()
        }
        learning_back_btn.setOnClickListener {
            dismissNotifications()
            // Go to prev activity (Architecture)
            startActivity(Intent(this, ArchitectureActivity::class.java))
        }
    }


    private fun learningComplete() {
        setLoadingBarsVisibility()
        setResultsVisibility()
    }


    private fun dismissNotifications() {
        val nManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        nManager.cancelAll()
    }


    private fun setLoadingBarsVisibility() {
        if (NeuralNetwork.isLearningInProgress) {
            learning_progress_linear_layout.visibility = View.VISIBLE
        } else {
            learning_progress_linear_layout.visibility = View.GONE
        }
    }


    private fun setResultsVisibility() {
        if (NeuralNetwork.isLearningDone) {
            learning_results_linear_layout.visibility = View.VISIBLE
            setResultsInformation()
        } else {
            learning_results_linear_layout.visibility = View.GONE
        }
    }


    @SuppressLint("SetTextI18n")
    private fun setResultsInformation() {
        learning_results_time_text.text = (NeuralNetwork.learningTime/60000).toString() + "min."
        learning_results_iterations_text.text = (NeuralNetwork.learningIterationsMade).toString()
        learning_results_training_value_text.text = NeuralNetwork.trainingSetAccuracy.toString() + "%"
        learning_results_test_value_text.text = NeuralNetwork.testSetAccuracy.toString() + "%"
        learning_results_cost_value_text.text = NeuralNetwork.currentCost.toString()
        setInsightText()
    }


    /**
     * Give comments and insight on the results. What can be improved.
     * So far this is prety basic, because there anr no one or two things that can be problematic
     */
    private fun setInsightText() {
        // High bias/variance
        if (NeuralNetwork.trainingSetAccuracy > GOOD_TRAINING_ACCURACY_THRESHOLD) { // Training set is doing ok
            if (NeuralNetwork.testSetAccuracy < NeuralNetwork.trainingSetAccuracy - GOOD_TEST_ACCURACY_DIFFERENCE_THRESHOLD) { // However, test is not doing ok -> high variance
                learning_results_insight_text.text = getString(R.string.learning_results_insight_variance)
            } else { // Both are ok
                learning_results_insight_text.text = getString(R.string.learning_results_insight_good)
            }
        } else { // Training set is NOT doing ok -> high bias or other problem
            learning_results_insight_text.text = getString(R.string.learning_results_insight_bias)
        }

        // Never reached optimum or reached it too fast
        if (NeuralNetwork.nbIterations == NeuralNetwork.learningIterationsMade) { // never reached
            learning_results_optimum_insight_text.text = getString(R.string.learning_results_insight_optimum)
        } else if (NeuralNetwork.trainingSetAccuracy < GOOD_TRAINING_ACCURACY_THRESHOLD) { // reached optimum, but accuracy is not good
            learning_results_optimum_insight_text.text = getString(R.string.learning_results_insight_optimum_learning_rate_bad)

        }

    }


    @SuppressLint("SetTextI18n")
    private fun updateProgress(intent: Intent?) {
        if (intent == null) return
        learning_progress_determinate_bar.progress = intent.getIntExtra(EXTRA_PROGRESS_NAME, 0)
        val cost = intent.getFloatExtra(EXTRA_COST_NAME, -1f)
        val iteration = intent.getIntExtra(EXTRA_ITERATION_NAME, -1)
        learning_cost_value_text.text = "Cost for iteration $iteration is $cost"

    }
}