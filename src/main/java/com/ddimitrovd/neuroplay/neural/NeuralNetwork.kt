package com.ddimitrovd.neuroplay.neural

import com.ddimitrovd.neuroplay.*
import com.ddimitrovd.neuroplay.data.DatasetManager
import kotlin.math.exp
import kotlin.math.ln
import kotlin.math.pow
import kotlin.math.sqrt
import kotlin.random.Random

// TODO: Consider putting all layers in one array instead of having the attributes inputLayer, outputLayer and hidden layers

/**
 * This object represents the architecture and the model of the used neural network. For optimisation and simplicity purposes
 * it makes uses of its attributes to make calculation directly on those values.
 */
object NeuralNetwork {

    private var nbUnitsPerHiddenLayer = DEFAULT_NUMBER_OF_HIDDEN_UNITS
    private var nbHiddenLayers = DEFAULT_NUMBER_OF_HIDDEN_LAYERS
    private var learningRate = DEFAULT_LEARNING_RATE
    private var regularizationParameter = DEFAULT_REGULARIZATION
    var nbIterations = DEFAULT_NUMBER_OF_ITERATIONS
        private set

    private var inputLayer: Array<FloatArray>? = null // training set, size:(examples x units in input)
    private var labels: Array<FloatArray>? = null // training set, size: (examples x classes)

    private var outputLayer: Array<FloatArray>? = null // hypothesis output, size: (examples x classes)

    private var hiddenLayers: Array<Array<FloatArray>?>? = null // activated values of hidden layers , size: (hidden layers x examples x units in layer)
    private var hiddenPowers: Array<Array<FloatArray>?>? = null // the so called z values that are given to the sigmoid to activate units (used in backprop, same size as hiddenLayers)

    private var weights: Array<Array<FloatArray>>? = null //all weights, size: (outgoingLayer x incomingLayer + bias)
    private var weightGrads: Array<Array<FloatArray>?>? = null //partial derivatives form backprop, size: (outgoingLayer x incomingLayer + bias)

    var isLearningDone = false
    var isLearningInProgress = false
    var isAborting = false
    var learningTime = 0L
    var learningIterationsMade = 0

    var trainingSetAccuracy = 0f
        private set
    var testSetAccuracy = 0f
        private set

    var currentCost = 0f
        private set


    /**
     * Activates layers with the current weights values and calculate the cost with the cost function
     * @return Current cost of the weights
     */
    fun calculateCostOfWeights(): Float {
        activateLayers()
        currentCost = costFunction()
        return currentCost
    }


    /**
     * Run backprop and then gradient descent (optimisation algorithm) on the gradients from backprop
     * @throws Exception if forwardpopagation hasn't been ran first (calculateCostOfWeights())
     */
    fun runOptimisationIteration() {
        //TODO : Throw
        backpropagation()
        runGradientDescent()
    }


    /**
     * Reverses the optimisation algorithm to get the previous weighs
     * This is used to avoid copying the weights after each iteration to allow rollback after optimum is reached
     */
    fun calculatePreviousWeights() {
        runGradientDescent(true)
    }


    /**
     * Use the learned model to predict the class of an input
     */
    fun predict(input: FloatArray): FloatArray {
        activateLayers(arrayOf(input))
        return outputLayer!![0]
    }


    /**
     * Set up parameters of the network architecture essential to the learning, If this is not called before learning
     * exceptions will be thrown upon use of null attributes
     * @param nbHiddenLayers the number of hidden layers in the network (obviously excluding input and output)
     * @param nbUnitsPerHiddenLayer array of the numbers of units per each corresponding hidden layer
     * @param nbIterations the number of iterations the optimisation algorithm will execute
     * @param learningRate the learning rate (alpha) (step size) for the optimisation algorithm
     * @param regularizationParameter the regularization parameter (lambda)
     */
    fun setUpNetwork(nbHiddenLayers: Int, nbUnitsPerHiddenLayer: IntArray, nbIterations: Int, learningRate: Float, regularizationParameter: Float) {
        reset()
        this.nbHiddenLayers = nbHiddenLayers
        this.nbUnitsPerHiddenLayer = nbUnitsPerHiddenLayer
        this.nbIterations = nbIterations
        this.learningRate = learningRate
        this.regularizationParameter = regularizationParameter
        initArrayOfHiddenLayers()
        makeScaledBiasedTrainingset()
        initWeights()
        initWeightsGrad()
    }


    /**
     * Reinitialize all relevant attributes of the object to initial state
     */
    private fun reset() {
        isLearningDone = false
        isLearningInProgress = false
        isAborting = false
        hiddenLayers = null
        outputLayer = null
        weights = null
        weightGrads = null
    }


    //region ----- Learning procedure ----------------------------------------------------------------------------------
    // The methods in this region access directly the object's attributes using the training set

    /**
     * Activates all layers and sets corresponding values to object's attributes
     * @param customInput instead of using the object's attribute inputLayer will use this input (used for prediction / test)
     */
    private fun activateLayers(customInput: Array<FloatArray>? = null) {
        // input to first hidden layer
        var activation = if (customInput == null) activationOfLayer(inputLayer, weights!![0]) else activationOfLayer(customInput, weights!![0])
        hiddenLayers!![0] = activation.first
        hiddenPowers!![0] = activation.second

        // hidden layers
        for (l in 0 until hiddenLayers!!.size - 1) { // only if there are more than 1 hidden layers
            activation = activationOfLayer(hiddenLayers!![l], weights!![l+1])
            hiddenLayers!![l+1] = activation.first
            hiddenPowers!![l+1] = activation.second
        }

        // output layer
        outputLayer = activationOfLayer(hiddenLayers!!.last(), weights!!.last(), false).first
    }


    /**
     * Computes the cost function J(Theta) with regularization (AKA loss)
     */
    private fun costFunction(): Float {
        val m = labels!!.size // number of examples
        val k = labels!![0].size // number of classes

        // Error sum
        var errSum = 0f
        for (i in 0 until m) { // Sum over all examples
            for (c in 0 until k) { // Sum over all classes
                errSum += labels!![i][c] * ln(outputLayer!![i][c]) + (1f - labels!![i][c]) * ln(1f - outputLayer!![i][c])
            }
        }

        // Regularization sum over the weights
        var regSum = 0f
        if (regularizationParameter > 0f) {
            for (t in weights!!) {
                for (i in t) {
                    for (j in 1 until i.size) {
                        regSum += i[j].pow(2)
                    }
                }
            }
        }

        // Return regularized cost function
        return (-1f / m) * errSum + (regularizationParameter / (2f * m)) * regSum
    }


    /**
     * Run backpropagation on the model to calculate the cost function partial derivatives that are necessary for optimisation
     * @see GradientCheck unit test verifies the implementation of the backprop algorithm
     */
    private fun backpropagation() {
        val m = labels!!.size // number of examples
        val k = labels!![0].size // number of classes
        val w = weights!!.size // number of weights matrices

        // Calculate the error terms needed to calculate the partial derivatives of the cost
        // bigDeltas size: classes x hidden units (size of corresponding weights)
        val bigDeltas = Array(w) {
                i -> Array(weights!![i].size) {
                    j -> FloatArray(weights!![i][j].size) { 0f } }}

        // Calculate the errors (delta) of units in output and consequently the hidden layers (backpropagate the error)
        // deltas is an array of vectors, each of length nbOfUnits in their respective outgoing layer
        // deltas is in reverse order - delta[0] is the one for output layer to last hidden layer and so on
        for (example in 0 until m) {
            val deltas = ArrayList<FloatArray>()

            // errors of units in last layer
            deltas.add(FloatArray(k) { unit -> outputLayer!![example][unit] - labels!![example][unit] }) // vector of size nbOfClasses

            // errors of units in hidden layers
            for (layer in nbHiddenLayers-1 downTo 0) {
                val sI = nbUnitsPerHiddenLayer[layer] // number of units in layer layer (incoming layer to the weights we are computing delta for)
                val sOut = if (layer == nbHiddenLayers-1) NUMBER_OF_CLASSES else nbUnitsPerHiddenLayer[layer+1] // number of units next layer (outgoing layer to the weights we are computing delta for)

                // delta = (weights transposed matrix product with delta of previous layer) * derivative of activation
                deltas.add(FloatArray(sI) { i ->
                    ((0 until sOut).map {
                            o -> deltas.last()[o] * weights!![layer+1][o][i + 1] }
                        .sum()) * sigmoidGradient(hiddenPowers!![layer]!![example][i + 1])
                }) // vector of size units in outgoing layer - bias
            }

            // Sum deltas in big deltas for each example
            for (i in bigDeltas.indices) {
                if (i == 0) {
                    bigDeltas[i] = sumInBigDelta(bigDeltas[i], deltas[deltas.size-1-i], inputLayer!![example])
                } else {
                    bigDeltas[i] = sumInBigDelta(bigDeltas[i], deltas[deltas.size-1-i], hiddenLayers!![i-1]!![example])
                }
            }
        }

        // calculate the partial derivatives of the cost using the big deltas and put them in weightGrads
        for (d in weightGrads!!.indices) {
            weightGrads!![d] = Array(bigDeltas[d].size) { i ->
                FloatArray(bigDeltas[d][0].size) { j ->
                    (1f / m) * bigDeltas[d][i][j] + isNotBias(j) * (regularizationParameter / m) * weights!![d][i][j]
                }
            }
        }
    }


    /**
     * Executes one iteration of gradient descent on all weight matrices
     */
    private fun runGradientDescent(isReversed: Boolean = false) {
        val reverse = if (isReversed) -1f else 1f
        for(l in weightGrads!!.indices) {
            for (i in weightGrads!![l]!!.indices) {
                for (j in weightGrads!![l]!![i].indices) {
                    weights!![l][i][j] += -learningRate * weightGrads!![l]!![i][j] * reverse
                }
            }
        }
    }

    //endregion


    //region ----- Initialization --------------------------------------------------------------------------------------

    private fun makeScaledBiasedTrainingset() {
        // Training set
        labels = DatasetManager.dataset.trainingLabels.toTypedArray()
        inputLayer = Array(DatasetManager.dataset.trainingData.size) { i ->
            FloatArray(DatasetManager.dataset.trainingData[0].size + 1) { j -> if (j == 0) 1f else DatasetManager.dataset.trainingData[i][j - 1] }
        }
    }


    private fun initWeights() {
        val weightsList = ArrayList<Array<FloatArray>>()

        for (i in 0..nbHiddenLayers) {
            when (i) {
                0 -> { //Input layer
                    val epsilon = epsilon(inputLayer!![0].size, nbUnitsPerHiddenLayer[i])
                    weightsList.add(Array(nbUnitsPerHiddenLayer[i]) { FloatArray(inputLayer!![0].size) { Random.nextDouble(-epsilon, epsilon).toFloat() } })

                }
                nbHiddenLayers -> { // Output layer
                    val epsilon = epsilon(nbUnitsPerHiddenLayer[i-1], NUMBER_OF_CLASSES)
                    weightsList.add(Array(NUMBER_OF_CLASSES) { FloatArray(nbUnitsPerHiddenLayer[i-1] + 1) { Random.nextDouble(-epsilon, epsilon).toFloat() } })

                }
                else -> { // All hidden layers
                    val epsilon = epsilon(nbUnitsPerHiddenLayer[i-1], nbUnitsPerHiddenLayer[i])
                    weightsList.add(Array(nbUnitsPerHiddenLayer[i]) { FloatArray(nbUnitsPerHiddenLayer[i-1] + 1) { Random.nextDouble(-epsilon, epsilon).toFloat() } })
                }
            }
        }

        weights = weightsList.toTypedArray()
    }


    private fun initWeightsGrad() {
        weightGrads = arrayOfNulls(nbHiddenLayers+1)
    }


    private fun initArrayOfHiddenLayers() {
        hiddenLayers = arrayOfNulls(nbHiddenLayers)
        hiddenPowers = arrayOfNulls(nbHiddenLayers)
    }


    /**
     * Checks if the network object is initialized
     */
    fun isInitialized(): Boolean {
        return !weights.isNullOrEmpty()
    }

    //endregion


    //region ----- Accuracy measuring ----------------------------------------------------------------------------------

    /**
     * Measure the acuracy of the learned model by testing it against the training set that it was using for learning a
     * unused test set
     */
    fun measureAccuracy() {
        // Trainingset
        var correct = 0
        for (e in outputLayer!!.indices) {
            if (outputLayer!![e].indexOf(outputLayer!![e].max()!!) == labels!![e].indexOf(labels!![e].max()!!)) {
                ++correct
            }
        }
        trainingSetAccuracy = (correct.toFloat()/outputLayer!!.size) * 100

        //Testset
        val test = getScaledBiasedTestset()
        // activate with test data
        activateLayers(test.first)
        // measure accuracy
        correct = 0
        for (e in outputLayer!!.indices) {
            if (outputLayer!![e].indexOf(outputLayer!![e].max()!!) == test.second[e].indexOf(test.second[e].max()!!)) {
                ++correct
            }
        }
        testSetAccuracy = (correct.toFloat()/outputLayer!!.size) * 100
    }


    /**
     * Get the testset form the current loaded dataset and add bias bias unit to it
     * @return a input matrix with bias units
     */
    private fun getScaledBiasedTestset() : Pair<Array<FloatArray>,Array<FloatArray>> {
        // Training set
        val labels = DatasetManager.dataset.testLabels.toTypedArray()
        val data = Array(DatasetManager.dataset.testData.size) { i ->
            FloatArray(DatasetManager.dataset.testData[0].size + 1) { j -> if (j == 0) 1f else DatasetManager.dataset.testData[i][j - 1] }
        }
        return Pair(data,labels)
    }

    //endregion


    //region ----- Generic for learning procedure procedure ------------------------------------------------------------
    // The methods in this region DO NOT access directly the object's attributes

    /**
     * Computes the activation matrix of a layer and saves powers of sigmoid function in order to not recalculate them in backprop
     * @param incomingLayer the previous layer matrix (examples x units in incoming layer)
     * @param weights mapping parameters (theta) from incoming to activated layer (units in activated layer x units in incoming layer)
     * @return Pair.first: matrix of activation values for the next layer (examples x units in activated layer + bias). Pair.second: matrix of z values (the sigmoid power) for the next layer  - Those values will be used in the backpropagation
     */
    private fun activationOfLayer(incomingLayer: Array<FloatArray>?, weights: Array<FloatArray>?, addBias: Boolean = true): Pair<Array<FloatArray>, Array<FloatArray>> {
        if (incomingLayer.isNullOrEmpty() || weights.isNullOrEmpty()) throw Exception("Activating with empty parameters")

        val bias = if (addBias) 1 else 0
        val m = incomingLayer.size // number of examples
        val sActivated = weights.size // number of units in layer being activated
        val sIncoming = incomingLayer[0].size // number of units in layer being activated

        val activation = Array(m) { FloatArray(sActivated + bias) { 1f } }
        val sigPower = Array(m) { FloatArray(sActivated + bias) { 1f } }
        for (example in 0 until m) {
            for (unit in 0 until sActivated) {
                // input[0].size = number of units in incomingLayer layer == weights[0].size
                sigPower[example][unit + bias] =
                    (0 until sIncoming).map { i -> weights[unit][i] * incomingLayer[example][i] }.sum()
                activation[example][unit + bias] = sigmoid(sigPower[example][unit + bias])
            }
        }
        return Pair(activation, sigPower)
    }


    /**
     * Calculates the bigDelta terms matrix that is used to compute the partial derivatives weightGrads in the last step of backprop
     * it is simply the matrix (vector) multiplication of the error (delta) and the activation value
     * @return Matrix of size (delta x activation)
     */
    private fun sumInBigDelta(bigDelta: Array<FloatArray>, delta: FloatArray, activation: FloatArray): Array<FloatArray> {
        for (i in bigDelta.indices) {
            for (j in bigDelta[0].indices) {
                bigDelta[i][j] += delta[i] * activation[j]
            }
        }
        return bigDelta
    }

    //endregion


    //region ----- utility functions -----------------------------------------------------------------------------------

    /**
     * Calculates the sigmoid function for a float
     */
    private fun sigmoid(z: Float): Float {
        return 1f / (1f + exp(-z))
    }


    /**
     * Calculates the sigmoid gradient (partial derivative) for a float
     */
    private fun sigmoidGradient(z: Float): Float {
        val sigZ = sigmoid(z)
        return sigZ * (1 - sigZ)
    }


    /**
     * Calculates the epsilon value for the random initialization of weights based on the size of incoming and outgoing layers
     * @param lIn the number of units in the incoming layer
     * @param lOut the number of units in the outgoing layer
     */
    private fun epsilon(lIn: Int, lOut: Int): Double {
        return sqrt(6.0) / sqrt(lIn.toDouble() + lOut.toDouble())
    }


    /**
     * Returns 1 if not bias unit (bias unit index is always 0)
     * Useful to control calculations where terms need to be included depending if bias unit or not
     */
    private fun isNotBias(int: Int): Int {
        return if (int > 0) 1 else 0
    }

    //endregion

    //region ----- Testing ---------------------------------------------------------------------------------------------
    // !!! NOTE: It is in general a BAD practice to include test access functions to the production code. Usually one tests
    // a class through its public methods, but due tho the programmatic complexity and the fact that we need tests that target
    // a specific method that is otherwise private (gradient check for backprop) I am adding test methods in the class.
    // I am leaving them in the production code just as a demonstration, they are practically dead code (another bad practice)

    /**
     * FOR TESTING ONLY!
     * Modify the indexed weight and return cost
     */
    fun wCostWithWeightModifTest(w: Int, row: Int, col: Int, offset: Float): Float {
        weights!![w][row][col] += offset
        activateLayers()
        val cost = costFunction()
        weights!![w][row][col] -= offset
        return cost
    }

    fun wBackpropGradTest(): Array<Array<FloatArray>?>? {
        activateLayers()
        costFunction()
        backpropagation()
        return weightGrads
    }

    //endregion
}