package com.ddimitrovd.neuroplay.neural

import android.app.IntentService
import android.app.NotificationManager
import android.content.Intent
import android.support.v4.app.NotificationCompat
import android.app.PendingIntent
import android.content.Context
import android.os.SystemClock
import com.ddimitrovd.neuroplay.*

/**
 * This service is responsible of optimising the neural network as a foreground service
 * It manipulates the trainingData in NeuralNetwork.kt and broadcasts progress on the learning routine
 */
class OptimizationService : IntentService("OptimizationService") {

    private val nBuilder = NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID) // Notification builder used in class
    private var bestCost = Float.MAX_VALUE // Used for optimum convergence detection

    override fun onHandleIntent(intent: Intent) {
        setNotification()
        startForeground(1, nBuilder.build())

        NeuralNetwork.isLearningInProgress = true
        runOptimisation(NeuralNetwork.nbIterations,intent.getBooleanExtra(EXTRA_ITERATION_OPTIMUM_CHECK,true))
        NeuralNetwork.isLearningInProgress = false
        NeuralNetwork.isLearningDone = true
        postNotificationComplete()
        broadcastCompletion()

        stopForeground(true)
        stopSelf()
    }


    /**
     * This will run the optimisation function of NeuralNetwork and after learning is complete run accuracy measurements
     * @param iterations the number of times to execute optimisation algorithm
     */
    private fun runOptimisation(iterations: Int, checkOptimum: Boolean = true) {
        var i = 1
        val startTime = SystemClock.elapsedRealtime()

        while (i <= iterations && !NeuralNetwork.isAborting){
            val cost = NeuralNetwork.calculateCostOfWeights()
            if (checkOptimum && isAtLocalOptimum(cost)) { // Has reached local optimum
                stopLearningAtOptimum()
                break
            }
            broadcastProgress(i,cost)
            NeuralNetwork.runOptimisationIteration()
            ++i
        }

        savePerformanceValues(i-1,startTime)
        NeuralNetwork.measureAccuracy()
    }


    /**
     * Reverses the values of the weights as they were in the previous iteration
     */
    private fun stopLearningAtOptimum() {
        NeuralNetwork.calculatePreviousWeights()
        NeuralNetwork.calculateCostOfWeights()

    }


    /**
     * Saves the values of interest regarding the optimisation perforance, such as elapsed time and executed iterations
     */
    private fun savePerformanceValues(iteration: Int, initialTime: Long) {
        NeuralNetwork.learningIterationsMade = iteration
        NeuralNetwork.learningTime = SystemClock.elapsedRealtime() - initialTime
    }


    /**
     * Tries to detect if the optimisation has reached a local optimum
     */
    private fun isAtLocalOptimum(cost: Float): Boolean {
        if (cost >= bestCost)
            return true
        else
            bestCost = cost
        return false
    }

    //region ----- Broadcasts ------------------------------------------------------------------------------------------

    /**
     * Broadcast progress of the learning algorithm by giving trainingData on progress, current cost and more
     */
    private fun broadcastProgress(iteration: Int, cost: Float) {
        val progress = ((iteration.toFloat()/NeuralNetwork.nbIterations.toFloat())*100f).toInt()

        updateNotificationProgress(progress)

        val broadcastIntent = Intent()
        broadcastIntent.action = BROADCAST_PROGRESS_NAME
        broadcastIntent.putExtra(EXTRA_ITERATION_NAME, iteration)
        broadcastIntent.putExtra(EXTRA_COST_NAME, cost)
        broadcastIntent.putExtra(EXTRA_PROGRESS_NAME, progress)
        sendBroadcast(broadcastIntent)
    }


    /**
     * Broadcasts that the optimisation routine is finished
     */
    private fun broadcastCompletion() {
        val broadcastIntent = Intent()
        broadcastIntent.action = BROADCAST_COMPLETED_NAME
        sendBroadcast(broadcastIntent)
    }

    //endregion


    //region ----- Notifications ---------------------------------------------------------------------------------------

    /**
     * Updates the non-dismissible foreground notification with progress on the determinate progress bar
     */
    private fun updateNotificationProgress(progress: Int) {
        nBuilder.setProgress(100,progress,false)
        val nManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        nManager.notify(1, nBuilder.build())
    }


    /**
     * Posts a dismissible notification to indicate that optimisation is finished
     */
    private fun postNotificationComplete() {
        nBuilder.setContentTitle(getString(R.string.notification_completed_title))
        nBuilder.setContentText(getString(R.string.notification_completed_text))
        nBuilder.setAutoCancel(true)
        nBuilder.setProgress(100,100, false)
        val nManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        nManager.notify(2, nBuilder.build())
    }


    /**
     * Builds the base for all notifications used by the service
     */
    private fun setNotification() {
        val notificationIntent = Intent(this, LearningActivity::class.java)
        val pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0)

        nBuilder
            .setContentTitle(getString(R.string.notification_title))
            .setContentText(getString(R.string.notification_text))
            .setContentIntent(pendingIntent)
            .setPriority(NotificationCompat.PRIORITY_LOW)
            .setSmallIcon(R.drawable.ic_neuroplay_icon)
            .setOnlyAlertOnce(true)
            .setProgress(0,0,true)
    }

    //endregion

}