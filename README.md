# Neuroplay

Google Play Store: https://play.google.com/store/apps/details?id=com.ddimitrovd.neuroplay

Youtube: https://www.youtube.com/watch?v=a7WiJkPnMZM&t=35s

Neuroplay is a small image recognition neural network builder. It is not very powerful nor very feature-rich. Actually, it is deliberately simple and limited. It can only work with a maximum of 3 classes! Its primary goal is to give an intuitive view of the inner-working of neural networks. To help you understand what is going on when the networks is learning a new dataset and to visualize how every layer and unit sees the intermediate data.

Play around with the network architecture and the dataset to see how different parameters influence the neural network.

Take a look at the code! Yes, it is OPEN SOURCE. You can head to the repository and see how everything works! Nuroplay doesn’t use complex machine learning libraries and advanced optimization algorithms. As well, I strive to keep the code well documented and clean. It is simple and easy to understand the fundamental basics of neural networks. 

It will take you no more than 5 minutes to learn to work with it and to start having fun! I strongly recommend cheeking the video tutorial. It is short and straight to the point!
You can either use a provided dataset or even better input your own by drawing it. Don't worry it won't be too tedious to gather a decent dataset every drawing you provide is augmented to multiply the images. You can collect a dataset of dimensions in the tens of thousands just in one minute!

I have great many ideas for cool features and inner-workings visualization tools, but I am only one guy, and this is a side project of mine. There is just that much time I can sacrifice for this app. I am aware that there are some bugs, limitations and room for optimization, but it's a free app. I hope you have fun with it and don't get too frustrated when thing go wrong

# !!! UNDER DEVELOPPEMENT !!!
As I mentioned there are lots of things I still want to do, and you will probably see that in the app with all the disabled buttons and warnings.
Things that I want to do in the nearest future if I have the time are:
- more optimization algorithms
- used defined augmentations
- built-in datasets
- improved performance / multithreading
- input visualisation
- better navigation between activities
- diagram visualisation of the neural network model, hypothesis and the individual units and weights
  + similar visualisation for backprop
- saving multiple custom datasets
- more customisable architecture
- and much much more

# Credits
Nuroplay is entirely developped by me (DDimitrovD), taking inspiration form the classes on Machine Learning of Andrew Ng 
and with the use of some great free assets and publicly available code:

Building simple drawing tool for android: 
https://android.jlelse.eu/a-guide-to-drawing-in-android-631237ab6e28

Icons: 
https://www.flaticon.com/authors/smashicons

# Privacy policy
https://gitlab.com/ddimitrovd/neuroplay/blob/master/privacy_policy.md
